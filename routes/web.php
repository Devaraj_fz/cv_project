<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });


Route::get('/','Website\ResumeController@index');
Route::get('/resume-builder','Website\ResumeController@resumeBuilder');

Route::get('/resume-example','Website\ResumeController@resumeExamples');

Route::get('/resume-templates','Website\ResumeController@resumeTemplates');

Route::get('/cv-builder','Website\ResumeController@cvBuilder');

Route::get('/cv-example','Website\ResumeController@cvExamples');

Route::get('/cv-templates','Website\ResumeController@resumeTemplates');


// user cv

Route::get('cv/dashboard','User\UserController@dashboard');

Route::get('user/login','User\UserController@login');

Route::get('user/register','User\UserController@register');

