@extends('website.layouts.master')


@section('content')

<link rel="stylesheet" href="{{asset('resources/views/website/assets/css/style1.css')}}">

	<main class="resume-builder">
		<section class="jumbotron section--border">
			<div class="grid">
				<div class="jumbotron__grid">
					<div class="jumbotron__container jumbotron__container--default">
						<div class="jumbotron__content ">
							<span class="jumbotron__supertitle">Fast. Easy. Effective.</span>
							<h1 class="jumbotron__title">Cv. The Best Resume Builder Online.</h1>
							<p class="jumbotron__subtitle text--width-xsmall">Writing a resume is a daunting task: stressful, confusing, and takes too much time. But not anymore — Let Cv lead the way.</p>
							<a class="button button--big button--red mt-35--phone mt-50--tablet" href="#" title="Create Your Resume Now">Create Your Resume Now</a>
						</div>
					</div>
				</div>
			</div>
		</section>


		<section class="section--white section--border section--small mediabar__section">
			<a class="mediabar grid" href="#" title="Cv in the media">

			<div class="mediabar__icons">
				<svg class="mediabar__item" viewbox="0 0 871 154">
				<path d="M12.44 94.17V49.25H0V36.97l12.44-1.67V19.98l26.84-3.77v19.1h17.8v14h-17.8v42.6c0 8 2.23 12.1 10.1 12.1 2.8 0 6.62-.3 8.57-.7v10.39c-4.3 2.54-13.5 4.7-22 4.7-16 0-23.5-7.05-23.5-24.23h-.01zm178.95-51.74c6.74 0 10.38 1.2 10.38 25.02l-22.5 1.32c.3-21.6 3.56-26.34 12.12-26.34zm-11.9 36.03h48.38c0-34.06-11.7-44.92-36.03-44.92-24.17 0-40.03 15.56-40.03 42.28 0 27.6 14.38 42.87 42.26 42.87 14.98 0 27.08-6.7 30.84-11.8v-7.9c-5.63 1.7-11.42 2.8-21.8 2.8-14.65 0-22.2-7.6-23.62-23.4v.07zM60.27 109.3l8.6-3.96v-88.3l-9.05-4.2V5.47L93.28.07h2.27v44.18h.9c7.3-6.6 17.1-10.72 28-10.72 13.7 0 20.5 6.82 20.5 21.7v50.1l8.7 3.97v7.47h-44.3v-7.47l8.7-3.97V61.3c0-8.24-3.6-11.32-10.9-11.32-4 0-7.8.73-11.1 2.06v53.3l8.7 3.98v7.47H60.34v-7.5l-.07.01zm587.17-70.47v7.38l9.04 4.2v55l-8.62 4v7.5h44.23v-7.4l-8.62-4v-72h-2.27l-33.76 5.39v-.07zm91.7 62.25V79.15l-6.28.6c-6.25.6-11.37 4.75-11.37 12.52 0 8.95 4.92 11.9 10.1 11.9 3 0 6.05-1.48 7.54-3.1l.01.01zM720.1 44.44L714.4 61h-13.96V39.7c7.54-2.34 16.73-6.16 30.96-6.16 21.65 0 34.66 6.4 34.66 24.6v47.2l8.6 4v6.12c-2.64 1.42-8.62 3.23-15.15 3.23-8.45 0-16.03-2.7-17.28-11.5h-.8c-3.6 7.2-13.2 11.6-23.3 11.6-14.4 0-23.5-9.3-23.5-22.8 0-16.6 10.9-19.3 28.7-23l15.5-2.7V59.7c0-10.42-3.2-15.6-13.5-15.6-1.4 0-3.9.18-5.5.34h.27zm56.76 64.87l8.6-3.9v-55l-9.05-4.2v-7.3l33.47-5.3h2.27v10.8h.9c7.3-6.6 17.1-10.7 28-10.7 13.7 0 20.5 6.8 20.5 21.7v50.1l8.7 4v7.5h-44.3v-7.4l8.7-3.92V61.3c0-8.24-3.6-11.32-10.9-11.32-4 0-7.8.73-11.1 2.06v53.3l8.6 3.98v7.47h-44.2v-7.5l-.19.02zm-167-92.2l-9.02-4.1v-7.4l33.74-5.4h2.27v102.7l8.6 4.7v7l-32 3.7h-2.2v-8.4h-.9c-3.4 4.3-10.3 8.8-20.4 8.8-14.8 0-32.5-9.4-32.5-40.5 0-32.7 18.1-44.5 39.6-44.5 4.5 0 9.8 1 13.1 3.4v-20h-.29zm0 85.4V47.6c-2.1-1.6-4.3-2.93-9.2-2.93-8.44 0-14.9 7.6-14.9 30.5 0 20.06 5.1 28.8 15.78 28.8 4.15 0 6.48-.84 8.3-1.58l.02.12zm-334.74 41.3c-14.74 0-19.8-5.2-19.8-10.8 0-3.9 1.04-6.6 3.12-9.1h28.53c6.67 0 9.7 2 9.7 6.2.02 8.4-4.65 13.7-21.55 13.7zm.23-100.8c5.9 0 9.7 2.6 9.7 18.8s-3.81 17.89-9.61 17.89c-5.8 0-9.54-1.35-9.54-17.87 0-16.5 3.7-18.8 9.58-18.8l-.13-.02zm17.9 56.3h-29c-3.1 0-5.7-2.4-5.7-5.2 0-2.2 1.6-4.5 3.6-6 4.4 1.3 8 1.7 13.4 1.7 20.9 0 35.2-9.5 35.2-27.2 0-7.9-3.3-12.4-8.5-17v-.6l13.4 4.7 1.8.1V33.5h-1.5l-20.63 4.1h-1.15c-5.14-1.9-12.3-3.9-18.8-3.9-20.9 0-35.36 10.37-35.36 27.98 0 10.73 5.4 18.72 13.47 23.14v.85c-4.66 3.28-13.6 10.02-13.6 18.65 0 6.3 4.16 12.53 12.86 14.4v.87c-9.15 1.9-19.4 4.9-19.4 15.4 0 10.8 15.65 18.8 41.16 18.8 31.7 0 45.42-12.2 45.42-31.5 0-15.9-8.8-23.3-26.7-23.3l.03.32zm203.1-60.4v7.4l9.1 4.2v55l-8.6 4v7.5h46.59v-7.4l-11.1-4V60.3c4.6-2.2 10.65-2.94 17.9-2.94 2.38 0 5.2.3 6.83.73V34.4c-.9-.3-2.67-.44-4-.44-8.9 0-16.17 5.73-20.92 16.6h-.8V33.44h-2.3l-32.7 5.38v.09zm-37.2 62.3v-22l-6.2.6c-6.2.6-11.3 4.8-11.3 12.6 0 9 4.9 11.9 10.1 11.9 3 0 6.06-1.5 7.54-3.1h-.14zm-19.1-56.7l-5.7 16.6h-14V39.7c7.6-2.34 16.8-6.16 31-6.16 21.7 0 34.7 6.4 34.7 24.6v47.2l8.6 4v6.12c-2.6 1.42-8.6 3.23-15.1 3.23-8.4 0-16-2.7-17.2-11.5h-.8c-3.6 7.2-13.2 11.6-23.24 11.6-14.37 0-23.4-9.3-23.4-22.8 0-16.6 10.97-19.3 28.76-23l15.58-2.7V59.7c0-10.42-3.14-15.6-13.5-15.6-1.35 0-3.9.18-5.52.34l-.18.07zm-111.4 5.9l-9-4.2v-7.3l33.8-5.3h2.3V90.7c0 9.25 4.53 10.9 11.35 10.9 5 0 7-.9 9.7-1.93V50.4l-9.1-4.2v-7.37l33.7-5.38h2.2v69.4l9 4.7v7.04l-32.8 3.6h-2.36l-.03-10.8h-.9c-5.18 6.4-13.73 11.1-24.7 11.1-16.3 0-23.27-11.6-23.27-25.7V50.4l.11.01zM669.44 0c8 0 14.45 6.47 14.45 14.46 0 7.98-6.47 14.46-14.46 14.46-8 0-14.5-6.4-14.5-14.4s6.5-14.4 14.5-14.4l.01-.12zm196.5 112.36l17.24.04-17.24-.04z"></path></svg> 
				<svg class="mediabar__item" viewbox="0 0 720.1 84.2">
				<g id="layer1" transform="translate(37.193 -635.976)">
					<g id="layer1-3" transform="translate(-68.571 182.857)">
						<path class="st2" d="M124.479 535.62h26.3l14.1-80.7h-26.3l-5.2 28.9h-22.8l5.2-28.9h-26.3l-14.1 80.7h26.3l5.3-30.2h22.8l-5.3 30.2z" id="path3348"></path>
						<path class="st2" d="M179.779 454.82l-8 44.9c-.7 3.5-1 6.9-1 10.1 0 23.8 20.8 27.4 33.3 27.4 26.2 0 37.5-8.7 41.6-32.2l8.8-50.3h-26.3l-7.5 42.2c-2.4 13.2-4.1 20.1-13.9 20.1-6.4 0-9.5-3.4-9.5-10.4 0-2.7.4-6 1.2-10.1l7.6-41.8h-26.3z" id="path3352"></path>
						<path class="st2" d="M393.479 535.62h26.3l3.7-20.8h12.4c22.9 0 35.6-12.4 35.6-35 0-15.9-11-25-30.3-25h-33.6l-14.1 80.8zm41-40.7h-7.6l3.5-19h7.1c5.8 0 8.9 2.9 8.9 8.2 0 6.7-4.5 10.8-11.9 10.8z" id="path3356"></path>
						<path class="st2" d="M522.579 453.12c-28.6 0-45.7 18.5-45.7 49.6 0 21.6 13.6 34.6 36.4 34.6 28.6 0 45.7-18.5 45.7-49.6.1-21.7-13.5-34.6-36.4-34.6zm-7.7 63.3c-6.8 0-10.8-4.9-10.8-13.1 0-3.1.3-5.8.9-8.9 2-10.6 5.4-20.3 16.1-20.3 6.8 0 10.8 4.9 10.8 13.1 0 3.1-.3 5.8-.9 8.9-2 10.5-5.4 20.3-16.1 20.3z" id="path3360"></path>
						<path class="st2" d="M650.479 535.62h26.3l10.4-59.1h19.8l3.8-21.6h-67.3l-3.8 21.6h21l-10.2 59.1z" id="path3364"></path>
						<path class="st2" d="M606.879 484.72c-8.3-2.6-11.7-3.7-11.7-7.6 0-2.6 1.7-5.7 6.7-5.7 3.7 0 6.8 2.1 8 5.2l23.2-6.3c-2.7-11.4-12.5-17.2-29.4-17.2-31.8 0-34.3 21.3-34.3 27.8 0 13.2 7 21.3 22.2 25.5 4 1.1 8.6 2.3 8.6 6.7 0 3.5-2.6 5.7-7.1 5.7-4.1 0-8.5-2.4-10-6.6l-22.9 6.2c2.5 12 13.8 18.8 31.5 18.8 13.5 0 36.1-3.6 36.1-28 .1-12.2-6.8-20.2-20.9-24.5z" id="path3366"></path>
						<path class="st3" d="M60.679 535.62l14.2-80.7h-43.5v80.7h29.3z" id="path3370"></path>
						<path class="st3" d="M722.079 454.92l-14.1 80.7h43.5v-80.7h-29.4z" id="path3374"></path>
						<path class="st2" d="M254.5 61.6h22.1l3.6-20.3h-22.1l2.5-15h30.9l3.8-21.7h-57.2L224 85.4h26.3z" id="polygon3376" transform="translate(31.379 450.22)"></path>
						<path class="st2" d="M323.4 61.6h22.2l3.6-20.3h-22.1l2.5-15h30.9l3.8-21.7h-57.2L293 85.4h26.3z" id="polygon3378" transform="translate(31.379 450.22)"></path>
					</g>
				</g></svg> 
				
				<svg class="mediabar__item" viewbox="0 0 796 198">
				<path d="M30.4979 195.7731H0V1.9627l30.4979 1.9666v191.8438m49.9266 0h-30.498V57.5481l30.498 1.7212v136.5038zM65.1755 15.4893c9.5922 0 17.4625 7.8707 17.4625 17.4627 0 9.5933-7.8703 17.464-17.4625 17.464-9.5922 0-17.4624-7.8707-17.4624-17.464 0-9.592 7.8702-17.4627 17.4624-17.4627zm88.0498 69.8504v30.498h-22.8737v79.802h-30.498V44.02c0-23.1187 19.1839-44.0253 41.8117-44.0253 7.624 0 13.7734 2.2146 15.2494 3.1973L155.684 32.46h-14.0187c-6.1493 0-11.3137 5.1653-11.3137 11.56v41.3197h22.8737m42.8347 26.5626h22.8733v-14.264c0-6.3959-5.1653-11.5604-11.3146-11.5604-6.3947 0-11.5587 5.1645-11.5587 11.5604v14.264zm-30.4987-14.264c0-23.121 18.692-42.0583 42.0574-42.0583 23.12 0 41.812 18.9373 41.812 42.0583v29.5145c0 8.6073-6.8867 15.2475-15.248 15.2475H196.06v13.2822c0 6.3932 5.164 11.5599 11.5587 11.5599h29.2693l.984 25.8255-27.5467 4.1802c-26.564 4.4276-44.764-18.4469-44.764-41.5656V97.6383zm109.9494 98.1348V3.1921h19.6773v58.536c5.4107-1.7214 14.0187-2.4588 19.9213-2.4588 20.9067 0 37.1387 18.9382 37.1387 40.3367v96.1671h-19.184v-96.167c0-10.5768-8.608-18.9392-19.184-18.9392h-18.692v115.1062h-19.6773m148.7213-37.1386v-33.204h-18.9387c-10.5746 0-18.9373 8.363-18.9373 18.9374v14.2666c0 10.5746 8.3627 19.1828 18.9373 19.1828 10.5774 0 18.9387-8.6082 18.9387-19.1828zm0-62.2265c0-11.0683-5.1653-19.923-19.184-18.4459h.2453c-7.8706.2454-17.9533 1.2292-26.316 1.721-.2453-7.1328-.492-9.8382-.7373-15.2484 6.64-2.46 16.232-4.92 24.3493-5.1654 24.5947-.492 40.828 19.4304 40.828 40.8272v58.538c0 21.1506-16.9706 38.6136-38.124 38.6136-21.1506 0-38.6133-17.463-38.6133-38.6136v-12.052c0-21.152 14.5107-37.1385 38.6133-37.1385 9.1014 0 13.7734.9839 18.9387 2.4583V96.408zm74.2867-37.1387c21.1533 0 38.6146 17.2158 38.6146 38.6143v2.4599c-7.624.2462-13.7733.2462-19.4293.4916v-2.9515c0-10.3297-8.6093-18.9376-19.1853-18.9376-10.3294 0-18.6934 8.6079-18.6934 18.9376v61.4885c0 10.8214 8.364 18.9386 18.6934 18.9386 10.3306 0 18.9386-7.871 19.1853-18.2016 9.5907.739 18.692 1.2306 19.4293 1.2306-.984 20.4136-18.2 36.647-38.6146 36.647-21.1507 0-38.3694-17.2172-38.3694-38.6146V97.8836c0-21.3985 17.2187-38.6143 38.3694-38.6143m116.136 136.5038l-37.384-65.1786c-3.6907-6.3933-3.6907-7.1318.7373-14.7568l32.9573-54.6004h22.3827l-37.14 62.964 40.0907 71.5718h-21.644zm-42.304-192.089v192.089h-19.676V3.6841h19.676zm87.768 115.1052h37.8773V98.1288c0-10.575-8.6094-19.1828-18.9387-19.1828-10.576 0-18.9387 8.6079-18.9387 19.1828v20.6605zm-19.676-20.6605c0-21.3968 17.2173-38.8595 38.6146-38.8595 21.1507 0 38.3693 17.4627 38.3693 38.8595v30.2532c0 5.6577-4.428 9.838-9.8386 9.838h-47.4693v20.9052c0 10.576 8.3626 19.1855 18.9386 19.1855h24.8413c.2454 6.3932.492 10.8229.984 15.2479l-23.612 3.6895c-24.1026 4.1813-40.828-16.724-40.828-38.1229V98.1288zm113.9186 97.6443H734.684V99.8512c0-21.3984 16.7253-41.3192 41.32-40.8285 8.3626.2466 13.528 2.2146 19.1853 4.92-.2467 5.9022-.492 10.8216-.7373 15.2486l-21.3987-.4916c-10.3307 0-18.692 11.0682-18.692 21.1515v95.922"></path>
			</svg> 
				
				<svg class="mediabar__item" viewbox="0 0 698 104">
				<path d="M18 37.5c6-5 11-7 18-8 16 0 30 11 34 28 5 20-7 40-27 44-19 4-38-7-42-27-1-4-1-8-1-12v-58c0-4 1-5 4-4h11c3 0 3 0 3 3v34zm36 29v-3c-1-8-7-15-14-17-8-2-15 2-19 9s-4 14 0 21c6 11 20 12 27 4 4-4 6-9 6-14zm296 7h-24c-3 0-3 0-2 2 5 11 20 15 28 7 2-1 3-2 5-1 4 2 8 3 12 4 2 1 2 2 1 3-7 10-17 14-29 14-14 1-28-8-34-22-5-15-1-31 10-42 11-9 28-12 41-5 14 8 19 24 18 38 0 2-1 2-3 2h-23zm-10-15h15c2 0 3 0 2-2-3-7-9-11-17-11-7 0-14 4-16 11-1 1-1 2 1 2h15zm-198-4v18c-1 19-14 31-33 30-18-1-29-12-29-30-1-12 0-25 0-38 0-2 0-3 2-3 4 0 8 1 12 0 3 0 4 1 4 4v36c0 10 7 16 17 13 6-1 10-6 10-13v-37c0-2 0-3 3-3h12c2 0 3 0 3 2-1 7-1 14-1 21zm93 23v-17c1-13 6-23 18-28s24-4 35 4c7 6 9 14 10 22v39c0 3-1 3-3 3h-13c-2 0-3-1-3-3v-37c0-10-6-15-16-13-6 1-10 6-10 13v36c0 4-1 5-5 4h-11c-2 0-3 0-3-2 1-7 1-14 1-21zm463 6v15c0 2 0 3-2 3-4 0-9 1-11-1s0-6 0-10v-20c0-7-4-11-10-11-7 0-11 4-11 10v28c0 3 0 3-3 3-4 0-8 2-10 0s-1-7-1-10v-20c0-7-4-11-10-11-7 0-11 4-11 11v28c0 2 0 3-3 3-3-1-7 1-10-1-2-1-1-6-1-10v-21c1-10 5-17 14-21s18-3 26 4c1 1 2 1 4 0 7-7 17-8 26-4s13 12 13 21v14zm-293 19c-10 0-17-3-24-9-1-2-2-3 0-5 3-2 5-4 7-7 2-2 3-2 4-1 3 3 6 5 9 6s6 1 8-1c4-1 4-5 2-8-2-2-5-4-7-4-4-3-8-4-12-7-8-6-12-14-10-23 3-8 10-14 20-14 10-1 17 2 23 9 2 1 2 2 0 4l-2 2c-2 2-4 5-6 6-3 1-4-3-7-4-2-1-5-2-8-1-2 0-3 1-4 3 0 2 1 3 3 4s5 3 7 4c4 2 7 4 11 6 8 5 11 12 10 22-2 9-9 15-18 17-3 1-5 1-6 1zm53 0c-9 0-16-3-23-10-1-1-1-2 0-4 3-2 6-5 8-8 1-1 2-1 3 0 3 3 5 4 8 5 2 1 3 2 5 2 4 0 7-2 8-5s-1-5-4-7c-3-1-6-3-9-5-4-2-8-4-11-7-11-11-7-29 8-33 11-4 21-1 30 8 2 2 2 2 0 4-3 2-5 5-8 7-1 1-2 1-3 0-2-2-4-3-6-4s-5-1-7 0c-1 0-3 1-3 3-1 2 1 4 2 5 3 1 5 3 8 4 3 2 7 3 10 6 8 5 11 13 9 22s-8 14-17 16c-3 1-6 1-8 1zm-286 0c-9 0-16-3-22-10-2-1-2-2 0-4s5-5 7-8c1-1 2-1 3 0 2 2 5 4 7 5s4 2 6 2c3 0 6-1 7-5 1-3-1-5-3-6-3-3-7-4-10-6-4-2-7-4-10-7-11-10-7-27 8-33 10-3 22 0 29 8 2 2 2 3 0 5s-4 4-7 6c-1 2-2 2-3 0-2-1-4-3-7-3-2-1-4-2-6-1s-4 1-4 4c0 2 1 3 3 4s4 3 7 4c3 2 7 3 11 6 8 6 11 13 9 23-2 9-9 14-18 16h-7zm437-28c0 16-12 28-27 28s-27-13-27-29c0-15 13-28 27-28 16 1 27 13 27 29zm-40 1c0 3 1 7 4 10 5 6 14 6 19 0s5-16 0-22c-4-4-9-6-14-4s-9 8-9 16zm-20-12c-11-6-20-6-26 1-5 6-5 16 1 21 6 6 16 6 25 0v14c0 1 0 1-1 2-14 7-35-2-41-17-6-17 6-35 24-38 5-1 11 0 16 1 2 1 3 2 3 4-1 4-1 8-1 12zm-324 3v31c0 3 0 4-3 4-4-1-8 0-12 0-2 0-2-1-2-3v-65c0-1 0-2 2-2h13c2 0 3 1 3 3-1 11-1 21-1 32zm-9-41c-2 0-6 1-8-1s0-5 0-8c0-8 0-8 8-8h8c1 0 2 0 2 2v13c0 2-1 2-3 2h-7zm287 69c0 3-3 7-7 7s-8-4-8-8 4-7 8-7 7 4 7 8z"></path>
			</svg> 
				
				<svg class="mediabar__item" viewbox="0 0 1186 106">
				<path d="M450.6 2.1c-13.2 1.5-25.8 10.3-32.4 22.7-4.3 8.1-6.2 16.1-6.2 26.2 0 14.2 3.7 24.2 12.2 33.5 15.4 16.9 46.7 18 62.1 2.1 2.4-2.5 5-5.9 5.7-7.6 1.1-2.7 1-3.3-.5-5-2.3-2.6-2.9-2.5-4.3.4-1.9 4.3-8.5 10.8-12.8 12.7-5.1 2.3-13.8 3.4-19.8 2.5-6.2-1-15.3-7.2-19.2-13.3-5.3-8.1-6.9-14.4-6.9-26.8 0-13.2 2.5-22.4 8.3-30 5.7-7.5 11.9-10.9 20.9-11.3 6.4-.4 8-.1 12.8 2.2 6.2 3.1 10.6 8.4 13.1 15.8 1.3 4 2 4.8 4 4.8h2.4l-.6-13.3c-.4-7.2-1-13.6-1.4-14-.4-.4-1.9-.3-3.3.4-2.1 1-4.2.9-10.4-.4-10.8-2.2-15.7-2.5-23.7-1.6zM1133.5 2.3c-16.5 5.5-23.6 19.3-17.1 33.3 3.1 6.6 8.3 10.9 21.9 17.9 20.3 10.5 23.7 13.8 23.7 23.2 0 9.2-6.5 14.3-18.3 14.3-11.5 0-21.3-7.4-24.9-18.8-1-3.4-1.7-4.2-3.3-4-2.1.3-2.6 2.5-3.8 20-.5 7.1-.4 7.8 1.3 7.8.9 0 2.3-.7 3-1.5 1.7-2.1 3.5-1.9 12.6 1.1 6.8 2.2 9.2 2.5 17.4 2.2 12-.5 18.3-3.1 24.4-9.9 9.2-10.2 8.6-24.1-1.6-33.9-2-2-9.6-6.8-16.8-10.6-14-7.4-20.1-11.9-22.5-16.5-3.2-6.2-.6-14.2 5.6-17.5 4.7-2.4 14.4-2.1 20.3.6 5.5 2.6 9.1 6.6 12.2 13.7 1.7 3.9 2.9 5.3 4.4 5.3 1.2 0 2-.7 2-1.7 0-4.9-2.4-24.9-3-25.7-.5-.4-2.1.1-3.7 1.3-2.7 2-3 2-7.8.5-7-2.1-21-2.7-26-1.1zM264.1 3.8c-.5 1-8.5 18.9-17.6 39.7-9.1 20.7-17.8 39.6-19.3 41.9-1.6 2.5-4.4 4.9-6.9 6.1-7.8 3.7-5.8 4.5 11.2 4.5 11.7 0 15.5-.3 15.5-1.3 0-.6-2.1-1.9-4.7-2.7-2.5-.8-5.1-2.2-5.6-3-.7-1.1.3-4.7 3.4-12.3l4.2-10.7h36.4l3.1 7.9c4.6 11.7 4.9 14.6 2 16.4-1.2.9-3.5 1.9-5 2.2-1.6.4-2.8 1.3-2.8 2.1 0 1.2 6.1 1.4 37 1.4 30.8 0 37-.2 37-1.4 0-.8-1.9-1.8-4.4-2.5-2.5-.6-5.7-2.1-7.1-3.4l-2.6-2.2.3-31.2.3-31.3 28 36c22 28.3 28.5 36 30.3 36h2.2V55.2c0-30.5.3-41.3 1.3-43.2.8-1.7 2.9-3.1 6.2-4.2 2.9-.9 5.1-2.3 5.3-3.2.3-1.5-1.4-1.6-16.5-1.4-19.5.3-20.4.9-9.5 5.4 2.6 1 4.4 2.5 4.8 3.8.2 1.2.3 13.6.2 27.7l-.3 25.5-24-31-24.1-31.1H330c-13 0-15.9 1.1-9.8 3.6 1.7.7 4.7 2.6 6.5 4.2l3.3 2.9v72.9l-2.5 2.4c-2 2.1-3.4 2.5-8 2.5-7 0-10.4-1.7-13.8-6.9-2.5-3.8-15.6-34.1-29.5-68.3-5.3-12.9-6.1-14.3-8.5-14.6-1.7-.2-2.9.3-3.6 1.6zm5.7 36.8c3.3 7.8 6.3 15.1 6.7 16.3.6 2.1.5 2.1-14 2.1-13 0-14.6-.2-14.1-1.6 4.4-11.4 14.1-32.3 14.7-31.7.4.4 3.5 7.2 6.7 14.9zM588.1 4.2c-5 12.7-34.6 78-36.8 81-1.5 2.3-4.2 4.7-6 5.5-4.8 2-12.6 1.7-15.4-.6l-2.4-1.9v-75l2.3-2.5c1.3-1.4 4.4-3 6.9-3.6 2.7-.7 4.3-1.6 4-2.4-.3-.9-5.9-1.3-21.1-1.5-18.9-.2-20.7-.1-20.4 1.4.2 1 2.2 2.2 5 3.1 8 2.3 7.8 1 7.8 41.5 0 25-.3 36.5-1.2 38.3-.8 1.8-2.6 3-6.5 4.2-2.9.9-5.3 2.3-5.3 3 0 1 6.9 1.3 36 1.3 30.1 0 36-.2 36-1.4 0-.8-1.5-1.7-3.4-2-1.9-.4-4.5-1.5-5.7-2.5l-2.2-1.7 4.4-11 4.5-10.9 18-.3 18.1-.2 3.7 9.5c2 5.2 3.6 10.4 3.6 11.5 0 2.2-3 4.5-7.2 5.5-1.6.4-2.8 1.3-2.8 2.1 0 1.2 8.9 1.4 56.8 1.4h56.8l1.8-11.8c1-6.4 2-12.5 2.3-13.5.3-1.3-.2-1.7-2.1-1.7-2.1 0-2.7.8-4.2 5.2-4.1 12.5-7.2 14.2-26.1 14.3-10.3 0-14.5-.4-15.5-1.3-1.9-1.9-2.4-71.6-.5-75.2.8-1.7 2.9-3.1 6.2-4.2 2.9-.9 5.1-2.3 5.3-3.2.3-1.5-1.7-1.6-20.4-1.4-22.4.3-26.1 1.3-16 4.2 3.6 1.1 6 2.4 6.8 3.9 1.8 3 2.4 64.3.8 72.2-1.4 6.7-4.1 8.5-12.3 8.5-10.1 0-10.3-.2-32.7-54.7C594.9 3 594.4 2.1 591.7 2c-1.9 0-3 .7-3.6 2.2zm6.5 37.8l7.1 17H572l1.3-3.3C583.1 32.8 586.5 25 587 25c.3 0 3.7 7.6 7.6 17zM1.2 4.6c.2.9 2.5 2.3 5.3 3.2 7.6 2.6 7.7 3 7.3 44.4-.3 39.1.2 36.5-8.3 39.7-9.3 3.5-7.1 4.1 15.9 4.1 17.7 0 21.6-.2 21.6-1.4 0-.8-1.8-1.8-4.4-2.5-2.4-.6-5.4-1.9-6.7-3-2.3-1.9-2.4-2.4-2.7-19.5L28.9 52h12.3c17 0 19.2 1.2 21.3 11.2.4 1.9 1.2 2.8 2.5 2.8 1.9 0 2-.7 2-17V32h-2.5c-2.1 0-2.5.5-2.5 3.2-.1 7.2-3.9 8.8-20.6 8.8H29V28.7C29 9 28.1 10 45.6 10c19.3 0 23.7 2 26.4 11.5.9 3.3 1.8 4.5 3.1 4.5 1.7 0 1.9-.8 1.9-6.3 0-3.4-.3-8.6-.6-11.5L75.7 3H38.3C4.5 3 .9 3.2 1.2 4.6zM82 4.5c0 .9 1.7 1.9 4.5 2.6 8.7 2.2 8.5 1 8.5 42.6 0 22.6-.4 37.1-1 38.3-.6 1.1-3.5 2.7-6.5 3.6-3 1-5.5 2.4-5.5 3.1 0 1 4.3 1.3 21 1.3s21-.3 21-1.3c0-.7-2.2-1.9-5-2.6-8.3-2.1-8-.6-8-42.2 0-41.8-.2-40.6 8.5-42.8 2.8-.7 4.5-1.7 4.5-2.6 0-1.3-3.1-1.5-21-1.5s-21 .2-21 1.5zM130 4.5c0 .8 1.5 2.1 3.3 2.8 1.8.8 4.7 3.1 6.5 5.2l3.3 3.7-.3 35.4c-.3 39.7.2 37.1-8.3 40.3-9 3.4-7 4.1 11.9 4.1 14.3 0 17.6-.3 17.6-1.4 0-.8-1.9-1.8-4.6-2.5-2.5-.7-5.6-2.3-7-3.6l-2.4-2.4V54.8l.1-31.3 2.4 3c1.4 1.6 14.1 17.9 28.2 36.2 19.8 25.4 26.3 33.2 28 33.2l2.2.1.3-41.8c.2-33.8.6-42.2 1.7-43.6.7-.9 3.8-2.3 6.8-3.2 9.5-2.8 6.3-3.9-12.1-4.2-15.2-.2-16.7-.1-16.4 1.4.2 1 2.1 2.2 4.9 3 2.6.8 5.3 2.1 6 3 1.1 1.3 1.5 7.5 1.6 28.8l.3 27.1-3-3.4c-1.6-1.9-12.7-16.2-24.7-31.7L154.5 3.1 142.3 3c-10.2 0-12.3.3-12.3 1.5zM762.6 9.2c-1.9 21.5-1.9 19.8.8 19.8 2.2 0 2.6-.6 3.6-5.4 2.1-10.3 7-13.6 20-13.6h8v38.5c0 43.7.5 41.3-8.5 43.6-2.6.7-4.5 1.7-4.5 2.5 0 1.1 3.8 1.4 21 1.4 17.4 0 21-.2 21-1.4 0-.9-1.6-1.7-4.1-2.1-5.1-.9-8.7-4.1-9-8 0-1.7-.1-19.1 0-38.8l.1-35.9 9.4.4c7.7.2 10 .7 12.3 2.4 3.5 2.6 5.4 6.5 6.1 12.1.4 3.7.8 4.3 2.9 4.3h2.4l-.3-12.8-.3-12.7-40.2-.3-40.1-.2-.6 6.2zM849 4.5c0 .8 1.7 1.9 4.3 2.6 2.3.6 5.2 2 6.5 3.2l2.2 2v36.9c0 23-.4 37.6-1 38.8-.6 1.1-3.5 2.7-6.5 3.6-3 1-5.5 2.4-5.5 3.1 0 1 4.3 1.3 21 1.3s21-.3 21-1.3c0-.7-2.2-1.9-5-2.6-8.3-2.1-8-.6-8-42.2 0-41.8-.2-40.6 8.5-42.8 2.8-.7 4.5-1.7 4.5-2.6 0-1.3-3.1-1.5-21-1.5-17.8 0-21 .2-21 1.5zM899.2 4.6c.2 1 2.1 2.2 4.9 3 2.6.8 5.2 2.1 6 3 1.5 1.8 1.5 12.3 0 54.1-.8 25.1-.8 25.1-8.7 27.3-2.4.7-4.4 1.9-4.4 2.6 0 1.1 3.2 1.4 16.5 1.4 13.4 0 16.5-.3 16.5-1.4 0-.8-1.8-1.8-4.2-2.4-8.6-2.2-8.3-1.3-8-25.4.2-11.7.7-26.7 1-33.3l.7-12 15.2 37c11.9 29.1 15.5 37 16.9 37 1.4 0 5-8.1 17.3-38.2 8.5-21.1 15.6-38.2 15.7-38 .1.1.7 14.7 1.3 32.5 1.4 37.8 1.6 36.7-7.2 39.7-3.1 1.1-5.7 2.5-5.7 3.2 0 1 4.6 1.3 20.5 1.3 22.4 0 25-.8 15.2-4.5-3.4-1.3-5.6-2.8-6.3-4.5-1.4-3.1-3.7-70.1-2.5-73.3 1.3-3.3 4.1-5.4 8.9-6.7 2.6-.6 4.2-1.6 3.9-2.3-.3-.9-4.6-1.2-15.2-1.2h-14.8l-13.1 32.2c-7.2 17.7-13.3 32.4-13.6 32.7-.3.3-6.6-14.2-14.1-32.2L928.4 3.5l-14.8-.3c-13.3-.2-14.7-.1-14.4 1.4z"></path>
				<path d="M1020.7 3.7c-1.5 1.4-.5 2.2 4.7 3.7 2.9.9 6 2.3 6.7 3.2 1.1 1.4 1.4 8.8 1.4 39 0 42 .6 39.1-8 42.3-10.1 3.8-7.7 4.1 35.3 4.1h41.1l.6-5.3c.4-2.8.7-9 .8-13.7.2-7.1-.1-8.5-1.5-8.8-1.2-.2-2.1 1.1-3.3 5-4.6 14-6.7 15.1-29.1 15.6-14.9.3-17.3.2-18.7-1.3-1.5-1.4-1.7-4.1-1.7-19.2V50.7l13.8.5c7.9.2 14.6.9 15.9 1.6 2.7 1.5 4.5 4.6 5.2 9.2.5 2.6 1.2 3.6 2.9 3.8l2.2.3V31h-2.5c-2.1 0-2.5.5-2.5 2.9 0 1.7-1 4.2-2.4 5.8l-2.4 2.8-15.1.3-15.1.3V10h17.5c22.3 0 25 .9 28 9.7.9 2.6 2 3.9 3.4 4.1 2 .3 2.1-.1 2.1-6.5 0-3.7-.3-8.5-.6-10.6l-.7-3.7H1060c-21.3 0-39 .3-39.3.7z"></path>
			</svg> 
				
				<svg class="mediabar__item" viewbox="0 0 352.5 91">
				<path d="M215.6 26.8c-5.5 0-9.8 1.5-14.4 3.7 0-12.9.2-24.1.5-28.4l-26.5 5.1v2.4l2.6.3c3.7.5 5.1 2.7 5.6 7.6.8 9.5.7 60.2 0 68.3 7.1 1.6 14.9 2.8 22.6 2.8 21 0 33.7-13 33.7-33.6-.1-16.4-10.3-28.2-24.1-28.2zm-9.2 57.7c-1.5 0-3.4-.2-4.5-.4-.4-5.7-.7-29.2-.6-49.7 2.4-.8 4.1-1 6.2-1 8.7 0 13.6 10.1 13.6 22.7-.1 16.2-6.1 28.4-14.7 28.4zM75.2 4H5.5v3.4l3.7.5c5.3.8 7.4 4.1 8 11.6 1.2 14.4 1 40.1 0 52.5-.6 7.5-2.7 10.8-8 11.6l-3.7.5v3.4h44.8v-3.4l-4.7-.5c-5.3-.8-7.4-4.1-8-11.6-.4-5.1-.7-13.7-.8-23.8l9.2.3c6.2.2 8.7 4.8 10.2 11.5h3.5V31.7h-3.5c-1.4 6.7-4 11.3-10.2 11.5l-9.3.3c0-13.3.3-26.1.9-33.8h13.8c10.7 0 16.5 6.8 20.1 18.9l4.1-1.1L75.2 4z"></path>
				<path d="M97.9 26.1c19.8 0 29.7 13.4 29.7 31.1 0 17.3-11.1 31.5-30.9 31.5C76.9 88.7 67 75.3 67 57.6c-.1-17.3 11-31.5 30.9-31.5zM96.8 30C88 30 85.6 41.8 85.6 57.5c0 15.2 3.9 27.5 12 27.5 8.8 0 11.2-11.8 11.2-27.5 0-15.3-3.9-27.5-12-27.5zM244.5 58c0-16.4 10.3-31.8 30.8-31.8 16.6 0 24.5 12.3 24.6 28.4h-37.1c-.4 14.6 7 25.4 21.2 25.4 6.2 0 9.6-1.5 13.4-4.4l1.7 2.1c-4.1 5.7-12.7 11.1-24.1 11.1-17.9-.1-30.5-12.7-30.5-30.8zm18.5-7.6l18.6-.4c.1-8.2-1.2-20-7.9-20-6.8 0-10.6 11.2-10.7 20.4zM346.8 29.3c-4.4-2-11-3.2-17.9-3.2C314 26.1 305 34.8 305 45c0 10.5 6.8 14.6 16.2 18 10.1 3.6 12.8 6.4 12.8 11.1 0 4.8-3.5 9.3-9.9 9.3-7.5 0-13.1-4.4-17.5-16.5l-3 .8.5 17.3c4.9 2 13.4 3.7 20.9 3.7 15.8 0 25.4-7.7 25.4-20.2 0-8.4-4.1-13-14.4-17-11.2-4.3-15.1-7.1-15.1-12.2 0-5.2 3.5-8.8 8.2-8.8 7.2 0 12.1 4.5 15.3 14.9l3-.8-.6-15.3zM179.2 27.8c-6.7-4-18.9-2.1-25 12.1l.3-13.8-26.5 5.1v2.4l2.6.3c3.7.5 5.1 2.7 5.6 7.6.8 9.4.7 26.3 0 34.5-.4 4.9-1.9 7.1-5.6 7.6l-2.6.3v3.4h36.8v-3.4l-4.8-.3c-3.7-.5-5.1-2.7-5.6-7.6-.7-7.5-.8-22.3-.2-32 1.8-2.9 10.4-5.3 18.4.1l6.6-16.3z"></path>
			</svg>
			</div></a>
		</section>


		<section class="section--white section--large overflow-x--hidden grid">
			<div class="grid__row resume-builder__two-columns mb-80--phone mb-100--desktop">
				<div class="grid__column--desktop--5 grid__column--tablet--12 grid__column--phone--4">
					<h2 class="mb-24--phone resume-builder__text-center--phone resume-builder__text-center--tablet">Build a resume that gets&nbsp;you&nbsp;hired—</h2>
					<p class="resume-builder__description resume-builder__text-center--phone mb-35--phone">You’re a lot of things. But you don’t have to be a resume writer. Don’t let your resume hold you back. With the Cv resume editor, you’ll create a document that gives you the confidence you deserve:</p>
				</div>

				<div class="grid__column--desktop--6 grid__column--offset--desktop-1 grid__column--tablet--12 grid__column--phone--4 text--center">
					<div class="resume-builder__image-wrapper resume-builder__two-columns--image1 mb-24--phone">
						<img alt="Build a resume that gets you hired." src="{{asset('/resources/views/website/assets/images/resumeimgs/templates@1x.png')}}">
					</div>
					<a class="resume-builder__two-columns-link" href="#" target="_blank">See more templates</a>
				</div>
			</div>

			<div class="grid__row">
				<section class="text--center grid__column--tablet--4 resume-builder__tile">
					<div class="tile tile--large">
						<div class="tile__wrapper">
							<img alt="Pick a resume template" class="tile__image" src="{{asset('/resources/views/website/assets/images/resumeimgs/pick-a-resume-template@1x.jpg')}}">
							
						</div>
						<h3 class="text--center tile__title">Pick a resume template.</h3>
						<p class="tile__description text--normal text--center text--width-small">Choose from 20+ professional resume templates in over 400 color variants.</p>
					</div>
				</section>

				<section class="text--center grid__column--tablet--4 resume-builder__tile">
					<div class="tile tile--large">
						<div class="tile__wrapper">
							<img alt="Fill in the blanks" class="tile__image" src="{{asset('/resources/views/website/assets/images/resumeimgs/make-it-yours@1x.jpg')}}">
						</div>
						<h3 class="text--center tile__title">Fill in the blanks.</h3>
						<p class="tile__description text--normal text--center text--width-small">Type in basic information. Use expert suggestions for everything else.</p>
					</div>
				</section>


				<section class="text--center grid__column--tablet--4 resume-builder__tile">
					<div class="tile tile--large">
						<div class="tile__wrapper">
							<img alt="Optimize your document" class="tile__image" src="{{asset('/resources/views/website/assets/images/resumeimgs/publish-your-resume-online@1x.jpg')}}">
							
						</div>
						<h3 class="text--center tile__title">Optimize your document.</h3>
						<p class="tile__description text--normal text--center text--width-small">Customize the look and feel. Make it sleek with no effort.</p>
					</div>
				</section>
				
			</div>
		</section>


		<section class="grid">
			<h2 class="text--center mb-50--desktop mb-24--phone">Your resume. Redefined.</h2>
			<div class="grid__row resume-builder__two-columns">
				<div class="grid__column--desktop--5 grid__column--tablet--4 grid__column--phone--4 text--center mb-35--phone">
					<div class="resume-builder__image-wrapper resume-builder__two-columns--image2">
						<img alt="Your resume. Redefined." class="resume-builder__two-columns-image" src="{{asset('/resources/views/website/assets/images/resumeimgs/resume-redefined@1x.png')}}">
					</div>
				</div>

				<div class="grid__column--desktop--6 grid__column--offset--desktop-1 grid__column--tablet--8 grid__column--phone--4">
					<div class="resume-builder__two-columns--mobile">
						<p class="resume-builder__description resume-builder__description--text-left text--width-xsmall">Cv resume maker. The solution you needed, but didn’t know about. Your resume will finally show your potential. Personalize dozens of resume templates.<br>
						<br>
						Get read-to-use suggestions—just pick and drop like online shopping. Impress recruiters with a professional design. Prove to employers who you are—the best candidate for the job.</p>
						<a class="button button--big button--red mt-35--phone mt-50--tablet resume-builder__button" href="#" title="Create your resume now">Create your resume now</a>
					</div>
				</div>
			</div>
		</section>


		<section class="section--medium section--shadow overflow-x--hidden mt-100--desktop">
			<div class="trustpilot">
				<h2 class="text--center mb-100--tablet mb-40--phone text--width-small">Rated excellent on Trustpilot</h2>
				<div class="trustpilot trustpilot-widget" data-businessunit-id="58a851be0000ff00059ce9cb" data-locale="en-US" data-schema-type="Organization" data-stars="4,5" data-style-height="149px" data-style-width="100%" data-template-id="53aa8912dec7e10d38f59f36" data-theme="light">
					<a href="#" rel="noopener noreferrer" target="_blank">Trustpilot</a>
				</div>
			</div>
		</section>


		<section class="overview section--medium mt-40--desktop">
			<div class="grid">
				<div class="overview__header overview__header--default">
					<h2 class="text--center h2 mb-40--phone">Cv Resume Builder</h2>
					<div class="grid__row">
						<div class="grid__column--desktop--4 grid__column--offset--desktop--2 grid__column--tablet--6">
							<p class="mt--40 text--width-xsmall">
								<span class="text--bold">You'll wish you found this program sooner.</span> Cv's resume builder helped millions of users create their resumes fast and land their dream jobs.</p>
						</div>

						<div class="grid__column--desktop--4 grid__column--tablet--6">
							<p class="mt--40 text--width-xsmall">Try it yourself. Just pick one of 20+ professional resume templates, fill in the blanks, and download. The resume wizard will guide you every step of the way.</p>
						</div>
					</div>

					<div class="testimony testimony--header--left">
						<span class="testimony__name">D. Ramos</span> 
						<svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady2.png')}}">
						<p class="testimony__content text--width-xsmall">I just got a great job at my dream company. I definitely could not have done it in such a timely manner without Cv. I send out 3–4 personalized applications at a time.</p>
					</div>

					<div class="testimony testimony--header--right">
						<span class="testimony__name">D. Ramos</span> <svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady2.png')}}">
						<p class="testimony__content text--width-xsmall">Being able to easily and quickly find resume items, duplicate resumes, and not have to put dozens of hours into making it look good has honestly saved me at least 50 hours.</p>
					</div>
				</div>

				<div class="grid__row resume-builder__two-columns overview__item">
					<div class="grid__column--desktop--6 grid__column--tablet--6 grid__column--phone--4 resume-builder__order--2">
						<div class="resume-builder__image-wrapper overview__image-wrapper--fast">
							<img alt="You’ll be done faster than your competition." class="overview__image--fast" src="{{asset('/resources/views/website/assets/images/resumeimgs/resume-builder-fast@1x.png')}}">
						</div>
					</div>

					<div class="grid__column--desktop--4 grid__column--tablet--5 grid__column--offset--tablet--1 grid__column--phone--4 grid__column--offset--desktop--2 resume-builder__order--1">
						<div>
							<h3>FAST</h3>
							<p class="text--width-xsmall mt-20--phone"><span class="text--bold">You’ll be done faster than your competition.</span> You should be out there enjoying your new career, not typing a resume. Cv’s resume helper will help you pick the right template, let you add information in a few clicks, and give you instant suggestions for what employers want. Upload your old resume or start from scratch. You’ll save time either way.</p>
							<ul class="overview__list">
								<li class="overview__list-item">
									<span class="overview__list-item--text">Access the online resume builder from anywhere at any time.</span>
								</li>
								<li class="overview__list-item">
									<span class="overview__list-item--text">Download or print your resume when you need to.</span>
								</li>
								<li class="overview__list-item">
									<span class="overview__list-item--text">Upload your past resume and give it a makeover.</span>
								</li>
							</ul>
						</div>
					</div>

					<div class="testimony testimony--fast--bottom resume-builder__order--3">
						<span class="testimony__name">Grace</span> <svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path>
					</svg> 
						<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady2.png')}}">
						<p class="testimony__content text--width-xsmall">Very friendly and easy to navigate through to the end of the project. So helpful. Redoing a resume that needed to be done in a hurry, Cv didn’t let me down.</p>
					</div>

					<div class="testimony testimony--fast--top resume-builder__order--3">
						<span class="testimony__name">customer</span> 
						<svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path>
					</svg> 
						<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/man.png')}}">
						<p class="testimony__content text--width-xsmall">It’s amazing how much time you can save with this tool and spend it on things that are important. Sure, you can do everything on your own, but what’s your time worth?</p>
					</div>
				</div>

				<div class="grid__row resume-builder__two-columns overview__item">
					<div class="grid__column--desktop--4 grid__column--tablet--5 grid__column--phone--4 resume-builder__order--1">
						<div>
							<h3>EASY</h3>
							<p class="text--width-xsmall mt-20--phone"><span class="text--bold">You’ll get personalized help and excellent support.</span> Writing a resume feels like a chore. And yet your future depends on it. Stop putting it off—the Cv resume creator is here to help. No more fighting with templates and software that never seem to work. No more second-guessing every word. Cv’s got you covered.</p>
							<ul class="overview__list">
								<li class="overview__list-item">
									<span class="overview__list-item--text">Get quality suggestions for what to include in your document.</span>
								</li>
								<li class="overview__list-item">
									<span class="overview__list-item--text">Copy, edit, and customize your resume with a few clicks.</span>
								</li>
								<li class="overview__list-item">
									<span class="overview__list-item--text">Add an easy to customize cover letter to maximize your chances.</span>
								</li>
							</ul>
						</div>
					</div>

					<div class="grid__column--desktop--3 grid__column--tablet--3 grid__column--phone--4 grid__column--offset--desktop--1 resume-builder__order--2">
						<div class="resume-builder__image-wrapper overview__image-wrapper--easy">
							<img alt="You’ll get personalized help and excellent support." class="overview__image--easy" src="{{asset('/resources/views/website/assets/images/resumeimgs/resume-easy@1x.png')}}">
						</div>
					</div>

					<div class="grid__column--tablet--4 grid__column--phone--4 resume-builder__order--2">
						<div class="testimony testimony--easy--top resume-builder__order--3">
							<span class="testimony__name">customer</span> <svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
							<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
							<img class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/man.png')}}">
						
							<p class="testimony__content text--width-xsmall">I really had fun creating my resume and cover letter. It was easy and I matched my skills with exactly what the employers have been looking for.</p>
						</div>

						<div class="testimony testimony--easy--middle resume-builder__order--3">
							<span class="testimony__name">Arna Meyer</span> <svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
							<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
							<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady2.png')}}">
							<p class="testimony__content text--width-xsmall">I was quite skeptical when I started the process of building a resume with their tool, but it’s so intuitive! Really does make the normally daunting task of preparing a resume easy, and dare I say—fun!</p>
						</div>

						<div class="testimony testimony--easy--bottom resume-builder__order--3">
							<span class="testimony__name">JB Pace</span> <svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
							<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
							<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/man3.png')}}">
							<p class="testimony__content text--width-xsmall">Using the Cv resume app to rebuild my resume was simple and actually enjoyable. The verbiage and explanations of skills available at the click of a button made my experience seamless.</p>
						</div>
					</div>
				</div>

															<!-- EFFECTIVE BANNER -->

				<div class="grid__row resume-builder__two-columns overview__item">
					<div class="grid__column--desktop--6 grid__column--tablet--6 grid__column--phone--4 resume-builder__order--2">
						<div class="resume-builder__image-wrapper overview__image-wrapper--effective">
							<img alt="You’ll get exactly what you need to land the job you want." class="overview__image--effective" src="{{asset('/resources/views/website/assets/images/resumeimgs/resume-builder-effective@1x.png')}}">
						</div>
					</div>
											

					<div class="grid__column--desktop--4 grid__column--tablet--5 grid__column--offset--tablet--1 grid__column--phone--4 grid__column--offset--desktop--2 resume-builder__order--1">
						<div>
							<h3>EFFECTIVE</h3>
							<p class="text--width-xsmall mt-20--phone">
								<span class="text--bold">You’ll get exactly what you need to land the job you want.</span> The Cv resume maker has built-in tips and ready-made content designed by resume experts. Our suggestions for each section will boost your chances of getting invited to interviews. Maximize your odds of landing the job now.</p>
							<ul class="overview__list">
								<li class="overview__list-item">
									<span class="overview__list-item--text">Create a resume a professional would charge you a few hundred dollars for.</span>
								</li>
								<li class="overview__list-item">
									<span class="overview__list-item--text">Learn from your competition and career experts what matters to employers.</span>
								</li>
								<li class="overview__list-item">
									<span class="overview__list-item--text">Increase your reply rate from future applications you submit with Cv.</span>
								</li>
							</ul>
							<p></p>
						</div>
					</div>

					<div class="testimony testimony--effective--top resume-builder__order--3">
						<span class="testimony__name">Customer</span> <svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/man.png')}}">
						<p class="testimony__content text--width-xsmall">Prior to using the Cv resume builder, I had applied to 10 different jobs and not one contacted me for an interview. Once I created my new resume using the Cv app, I applied to 3 more jobs, and the next day I was contacted with an interview date. I had no idea the detriment my past resume was causing me.</p>
					</div>

					<div class="testimony testimony--effective--bottom resume-builder__order--3">
						<span class="testimony__name">Amber</span> 
						<svg class="testimony__rating" height="15" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path>
					</svg> 
						<img alt="Client&#39;s avatar" class="testimony__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady2.png')}}">
				
						<p class="testimony__content text--width-xsmall">Easy to use and customize. Great for every type of resume. After submitting several resumes, I only got an interview from the company where I submitted the resume I created with Cv’s resume maker app.</p>
					</div>
				</div>
			</div>
		</section>

		<section class="grid section--large testimonies-section">
			<h2 class="testimonies__title">One word? HIRED</h2>
			<div class="grid__row">
				<div class="grid__column--desktop--4 grid__column--offset--desktop--2 grid__column--tablet--6">
					<p class="mt--40 text--width-xsmall"><span class="text--bold">You’ll finally get a job you want, not the one you have to take.</span> Break free from the vicious cycle of lowering expectations just to get any job.</p>
				</div>

				<div class="grid__column--desktop--4 grid__column--tablet--6">
					<p class="mt--40 text--width-xsmall">Start picking between offers you actually want. Apply the resume generator’s advice, optimize your application, and get that dream job.</p>
				</div>
			</div>

			<div class="testimonies grid__row">
				<div class="grid__column--desktop--3 grid__column--tablet--4 grid__column--phone--4 mb-28--tablet">
					<div class="testimonies__item">
						<span class="testimonies__name">Denise P.</span> <svg height="13" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimonies__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/man3.png')}}">
						<p class="testimonies__content text--width-xsmall">I utilized Cv’s builder app to the fullest, applying everything I learned, and even added some unique touches the resume software offered. Now, instead of stressing over what keyword or language to use on my resume, I can focus on which job offer to accept.</p>
					</div>
				</div>

				<div class="grid__column--desktop--3 grid__column--tablet--4 grid__column--phone--4 mb-28--tablet">
					<div class="testimonies__item">
						<span class="testimonies__name">David Birdsell Jr.</span> 
						<svg height="13" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimonies__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/man2.png')}}">
						<p class="testimonies__content text--width-xsmall">I went from fired to hired in less than 3 weeks. I was let go from my old job due to downsizing. With Cv, I was able to build custom resumes tailored to the jobs I was applying to quickly and easily. My last day was 3/9/18 and I had several offers by 3/25/18. Accepted an excellent job on 3/27/18. I can't thank you guys enough. You really helped save my family from some serious hardship.</p>
					</div>
				</div>

				<div class="grid__column--desktop--3 grid__column--tablet--4 grid__column--phone--4 mb-28--tablet">
					<div class="testimonies__item">
						<span class="testimonies__name">Jeanne</span> <svg height="13" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimonies__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady2.png')}}">
						<p class="testimonies__content text--width-xsmall">Cv is what will make you stand out. In less than a month and a half, I got contacted by six employers and went in for three interviews. They’ve all commented on how my résumé caught their eye, and how “visually pleasant” it is. The cover letter tool has been a tremendous help as well. It has made my life so much easier!</p>
					</div>
				</div>

				<div class="grid__column--desktop--3 grid__column--tablet--4 grid__column--phone--4 mb-28--tablet">
					<div class="testimonies__item">
						<span class="testimonies__name">Rania</span> <svg height="13" width="76" xmlns="http://www.w3.org/2000/svg">
						<path d="M7 .28L8.57 5.1h5.09L9.54 8.1l1.57 4.83L7 9.94l-4.11 2.99L4.46 8.1.34 5.1h5.09L7 .28zM22.4.28l1.57 4.83h5.08L24.94 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.1-2.99h5.08L22.4.28zM37.8.28l1.57 4.83h5.08L40.34 8.1l1.57 4.83-4.11-2.99-4.12 2.99 1.57-4.83-4.11-2.99h5.08L37.8.28zM53.2.28l1.57 4.83h5.08L55.74 8.1l1.57 4.83-4.12-2.99-4.1 2.99 1.56-4.83-4.11-2.99h5.08L53.2.28zM68.6.28l1.56 4.83h5.09L71.14 8.1l1.57 4.83-4.12-2.99-4.11 2.99 1.57-4.83-4.11-2.99h5.08L68.6.28z" fill="#3983FA"></path></svg> 
						<img alt="Client&#39;s avatar" class="testimonies__avatar" src="{{asset('/resources/views/website/assets/images/resumeimgs/lady.png')}}">
						<p class="testimonies__content text--width-xsmall">Cv saved my resume and my career. I was applying for jobs with my own made resume and kept failing... until I came to Cv. I enhanced my resume and was amazed how professional the resume builder and the tips are. Guess what? I started to receive phone calls and emails for interviews. I have a job offer in the spring!</p>
					</div>
				</div>
			</div>
			<p class="testimonies__subtitle">…and what will your story be?</p>
			<a class="button button--big button--red mt-35--phone testimonies__button" href="#" title="create your resume now">create your resume now</a>
		</section>

		<section class="section faq">
			<h2 class="text--center resume-builder__heading mb-50--desktop">FAQ</h2>
			<div class="faq text--center grid">
				<div class="faq__wrapper">
					<div class="faq__item">
						<h3 class="h4 mb-25--phone">What is a resume builder?</h3>
						<p>A resume builder is an online app or piece of software that provides users with interactive forms and templates that focus on resumes. The best applications of this type provide tips and suggestions to help you provide employers with the right kind of information. For example, the Cv resume builder features ready-made content tailored to the needs of specific job seekers. This makes wording each section easier in terms of communicating your value as an employee.</p>
					</div>

					<div class="faq__item">
						<h3 class="h4 mb-25--phone">Who is Cv resume builder for?</h3>
						<p>The Cv resume creator is a tool for anyone tired of fighting with formatting their application document in a word processor. It’s even more valuable to job seekers pressed for time or worried about how the wording of their resume reflects on them as candidates. Cv can dramatically cut down on the time it would take to create a resume on your own. We’ve heard success stories from job seekers in all industries. Interns, juniors, mid-level staff, all the way to c-suite executives. Even students writing resumes for college applications.</p>
					</div>

					<div class="faq__item">
						<h3 class="h4 mb-25--phone">Is Cv safe and legit?</h3>
						<p>Yes, Cv is a legit business you can learn more about on the 
							<a href="#" title="About Cv">About Cv</a> page. The application is safe thanks to a secure SSL https encrypted connection. Your personal data is processed according to stringent policies you can learn more about in the 
							<a href="#" title="Terms of use">terms of use</a>, <a href="privacy-policy.html" title="Privacy policy">privacy policy</a>, and 
							<a href="#" title="Cookie policy">cookie policy</a>.</p>
					</div>
					
					<div class="faq__item">
						<h3 class="h4 mb-25--phone">What features does the Cv creator offer?</h3>
						<p>Cv is actually a suite of tools designed to help you create application documents:</p>
						<ul class="faq__list">
							<li class="faq__list-item">
								<span class="faq__list-item--text">Resume and CV builder</span>
							</li>
							<li class="faq__list-item">
								<span class="faq__list-item--text">Cover letter builder</span>
							</li>
							<li class="faq__list-item">
								<span class="faq__list-item--text">Templates for various application documents</span>
							</li>
							<li class="faq__list-item">
								<span class="faq__list-item--text">Built-in content and suggestions</span>
							</li>
							<li class="faq__list-item">
								<span class="faq__list-item--text">Printable downloads in .txt, .pdf, and .doc</span>
							</li>
							<li class="faq__list-item">
								<span class="faq__list-item--text">Resume checker</span>
							</li>
							<li class="faq__list-item">
								<span class="faq__list-item--text">And almost a 1,000 articles available for free on the site</span>
							</li>
						</ul>
					</div>

					<div class="faq__item">
						<h3 class="h4 mb-25--phone">Is Cv resume builder free?</h3>
						<p>Cv resume builder is free to create a resume. The app follows the “try before you buy” credo—you pay once you’re satisfied with the results. Downloading a printable resume as a pdf or MS Word .doc file from Cv costs just 2.99 USD for two week access. Pricing may vary depending on the plan and subscription.</p>
					</div>
				</div>

				<div class="button button--big button--red faq__button mt-40--phone">
					<span class="faq__see-more">See more</span> 
					<span class="faq__see-less">See less</span>
				</div>
			</div>
		</section>


		<section class="section--triangles section--large text--center mt-100--tablet mt-80--phone">
			<div class="grid">
				<h3 class="text--white">Try Cv's professional resume builder now</h3>
				<a class="button button--big button--white mt-40--tablet mt-35--phone" href="#" title="land your dream job now">land your dream job now</a>
			</div>
		</section>
	</main>

@endsection