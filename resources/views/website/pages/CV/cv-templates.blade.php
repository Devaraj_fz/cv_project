@extends('website.layouts.master')


@section('content')

	<link rel="stylesheet" href="{{asset('resources/views/website/assets/css/resume-template.css')}}">



	<div class="cookie-policy cookie-policy--hidden text--center">
		<p class="cookie-policy__paragraph text--center">This site uses cookies to ensure you get the best experience on our website. To learn more visit our 
      <a href="#">Privacy Policy</a>
    </p>
      <span class="button button--medium button--white cookie-policy__button">Got it!</span>
	</div>
	<main>

		<section class="jumbotron">
			<div class="grid text--center">
				<h1 class="text--center jumbotron__title mt-55--phone mt-45--tablet mt-30--desktop">CV Templates</h1>
				<p class="jumbotron__subtitle text--center">Pick a resume template, fill it out, and format. Create a 
          <strong>professional resume in a few clicks</strong>. Just choose one of 18+ resume templates below, add ready-made content, download, and get the job.</p>
			</div>
		</section>

		<div class="template-list__wrapper">
			<nav class="navigation">
				<div class="grid navigation__wrapper">
					<div class="navigation__switcher-wrapper">
						<h2 class="navigation__switcher navigation__switcher--active">CV 
              <span class="navigation__switcher--hidden-phone">templates</span>
            </h2>
              <a class="navigation__switcher" href="#" title="Cover letter">Cover letter 
                <span class="navigation__switcher--hidden-phone">templates</span>
              </a> 
                <a class="navigation__switcher" href="#" title="CV">CV 
                  <span class="navigation__switcher--hidden-phone">templates</span>
                </a>
					</div>

					<div class="navigation__categories">
						<span class="navigation__category navigation__item" data-category-id="Recommended">Recommended</span> 
            <span class="navigation__category navigation__item navigation__category--active" data-category-id="All">All</span>
					</div>
				</div>
			</nav>


			<section class="template-list">
				<div class="grid">
					<div class="grid__row">
						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="SRZ1" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Cascade" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/cascade-3-duo-blue-navy-21@1x.png')}}" height="680"  width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div>
            </a>

							<h2 class="template__title">Cascade</h2>
							<p class="template__description">Professional resume template. Plenty of information, no clutter.</p>
						</section>

						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="SRZ2" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Concept" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/concept-10-classic-blue-navy-312@1x.png')}}" height="680"  width="481">
                <noscript>
                  <img alt="Professional CV Template Concept" class="example__image" height="680"  width="481">
                </noscript>
                <span class="button button--medium button--red template__button">Use this template</span>
							</div>
            </a>
							<h2 class="template__title">Concept</h2>
							<p class="template__description">Modern resume template. A timeline to show progression and icons to save space.</p>
						</section>

            
						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="SRZ3" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Crisp" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/crisp-15-classic-silver-dark-388@1x.png')}}" height="680" width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div>
            </a>
							<h2 class="template__title">Crisp</h2>
							<p class="template__description">Creative resume template. A perfect balance of graphics and whitespace.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="Recommended" data-template-id="SRZ4" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Cubic" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/cubic-20-trio-silver-dark-19@1x.png')}}" height="680"  width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div>
            </a>
							<h2 class="template__title">Cubic</h2>
							<p class="template__description">Perfect resume template. Readable no matter how much you write.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="Recommended" data-template-id="SRZ5" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Diamond" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/diamond-21-duo-silver-dark-999@1x.png')}}" height="680" width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div></a>
							<h2 class="template__title">Diamond</h2>
							<p class="template__description">Single column resume template. Work history is the focus, the job title the selling point.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="Recommended" data-template-id="SRZ6" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Enfold" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/enfold-18-duo-blue-navy-1165@1x.png')}}" height="680" width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div></a>
							<h2 class="template__title">Enfold</h2>
              
							<p class="template__description">Two column resume template. Focuses on your summary, draws attention to skills.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="SRZ7" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Iconic" class="example__image"  src="{{asset('resources/views/website/assets/templates/resume_templates/iconic-9-classic-silver-dark-276@1x.png')}}" height="680" width="481">
              </div>
            </a>
							<h2 class="template__title">Iconic</h2>
							<p class="template__description">Icon-based resume template. Section that pop with a little bit of graphic help.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="SRZ8" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Influx" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/influx-8-duo-silver-dark-971@1x.png')}}" height="680"  width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div>
            </a>
							<h2 class="template__title">Influx</h2>
							<p class="template__description">Elegant resume template. Tells a story, makes sure it gets heard.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="SRZ9" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
							<div class="template__image">
								<img alt="Professional CV Template Initials" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/initials-5-classic-blue-navy-228@1x.png')}}" height="680" width="481">
                <span class="button button--medium button--red template__button">Use this template</span>
							</div>
            </a>
							<h2 class="template__title">Initials</h2>
							<p class="template__description">Creative resume template. Initials for a personal touch, dash of color to highlight strengths.</p>
						</section>

						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="TRZ1" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank">
                <div class="template__image">
                  <img alt="Professional CV Template Minimo" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/minimo-4-classic-blue-navy-200@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Minimo</h2>
							<p class="template__description">Minimalistic resume template. Great readability with a surprisingly sleek finish.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="TRZ2" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank">
                <div class="template__image">
                  <img alt="Professional CV Template Modern" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/modern-57-classic-blue-navy-3568@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Modern</h2>
							<p class="template__description">Modern resume template. Elegant accents in the header, footer, and section titles.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="Recommended" data-template-id="TRZ3" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank">
                <div class="template__image">
                  <img alt="Professional CV Template Muse" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/muse-2-classic-blue-navy-172@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Muse</h2>
							<p class="template__description">Visual resume template. Two columns that help focus on your experience.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="TRZ4" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank">
                <div class="template__image">
                  <img alt="Professional CV Template Nanica" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/nanica-16-classic-silver-dark-416@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Nanica</h2>
							<p class="template__description">Traditional resume template. Neat layout that focuses on your career highlights.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="Recommended" data-template-id="TRZ5" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank"  rel="nofollow noopener noreferrer">
                <div class="template__image">
                  <img alt="Professional CV Template Newcast" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/newcast-14-classic-blue-navy-368@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Newcast</h2>
							<p class="template__description">Basic resume template. Standard design with a designer finish.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="TRZ6" data-template-type="cv">
							<a href="###" rel="nofollow noopener noreferrer" target="blank"  rel="nofollow noopener noreferrer">
                <div class="template__image">
                  <img alt="Professional CV Template Primo" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/newcast-14-classic-blue-navy-368@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Primo</h2>
							<p class="template__description">Infographic resume template. Blends classic structure with timelines and graphs.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="TRZ7" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank"  rel="nofollow noopener noreferrer">
                <div class="template__image">
                  <img alt="Professional CV Template Simple" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/simple-1-classic-blue-navy-144@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Simple</h2>
							<p class="template__description">Simple resume template. Excellent readability without being bland.</p>
						</section>


						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="" data-template-id="TRZ8" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank"  rel="nofollow noopener noreferrer">
                <div class="template__image">
                  <img alt="Professional CV Template Valera" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/valera-11-classic-silver-dark-332@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Valera</h2>
							<p class="template__description">Unique resume template. One-of-a-kind typography, classic structure to guide the gaze.</p>
						</section>

						<section class="template grid__column--phone--4 grid__column--tablet--6 grid__column--desktop--4" data-category-id="Recommended" data-template-id="TRZ9" data-template-type="cv">
              <a href="###" rel="nofollow noopener noreferrer" target="blank"  rel="nofollow noopener noreferrer">
                <div class="template__image">
                  <img alt="Professional CV Template Vibes" class="example__image" src="{{asset('resources/views/website/assets/templates/resume_templates/vibes-19-classic-blue-navy-452@1x.png')}}" height="680" width="481">
                  <span class="button button--medium button--red template__button">Use this template</span>
                </div>
              </a>
							<h2 class="template__title">Vibes</h2>
							<p class="template__description">Sleek resume template. A clean format with exciting details.</p>
						</section>

					</div>
				</div>
			</section>
		</div>

		<section class="section--shadow section--large overflow-x--hidden">
			<div class="trustpilot">
				<h2 class="text--center mb-100--tablet mb-40--phone text--width-small">Rated excellent on Trustpilot</h2>
				<div class="trustpilot trustpilot-widget" data-businessunit-id="58a851be0000ff00059ce9cb" data-locale="en-US" data-schema-type="Organization" data-stars="4,5" data-style-height="149px" data-style-width="100%" data-template-id="53aa8912dec7e10d38f59f36" data-theme="light">
					<a href="##" rel="noopener noreferrer" target="_blank">Trustpilot</a>
				</div>
			</div>
		</section>


		<section class="section--medium text--center">
			<div class="grid">
				<h2 class="h3">Need text samples of resume templates for a specific job?</h2>
        <a class="button button--big button--red mt-20--phone" href="#" title="See resume examples">See resume examples</a>
			</div>
		</section>


		<section class="section--medium text--center">
			<div class="grid">
				<h2 class="text--center mb-60--tablet mb-40--phone">Free CV Template to Copy and Use</h2>
				<div class="document">
					<p><strong>Contact Information</strong></p>
					<p>[Your Name]</p>
					<p>[Your Job Title]</p>
					<p>[Email&nbsp;Address]</p>
					<p>[Phone Number]</p>
					<p>[LinkedIn]</p>
					<p>[Optional: Personal Website, Twitter, Other Relevant Links]</p>
					<p><strong>CV Summary Statement</strong></p>
					<div>
						<p>Dependable/Detail-oriented/Creative[Your Job Title]&nbsp;with&nbsp;[X]&nbsp;years of experience in&nbsp;[Your Industry/Niche]. Helped&nbsp;[increase revenue/cut costs/train employees/other achievements]&nbsp;by&nbsp;[X]%. Looking to join&nbsp;[Company Name]&nbsp;to ensure[highest customer happiness scores/a steady boost in ROI/prompt project delivery/other metrics and KPIs you hope to deliver for the prospective employer].</p>
						<p><strong>Work Experience/Job Description</strong></p>
					</div>
					<p>[Your Job Title]</p>
					<p>[Company Name]</p>
					<p>[2012&ndash;2019]</p>
					<ul>
						<li>Use bullet points to describe your work history.</li>
						<li>Add up to 6 bullet points. Focus on what applies to the job you're trying to land, don't cram your resume with unnecessary details.</li>
						<li>Don't just list your responsibilities. Focus on your achievements!</li>
						<li>Maybe you saved your company money? Boosted sales? Optimized processes? Trained new employees? Show it off! Have a look:</li>
						<li>Responsible for&nbsp;[your responsibilities].</li>
						<li>[Boosted sales/cut costs]&nbsp;by&nbsp;[X]% through implementing a new system of&nbsp;[invoicing/project management/procurement, etc.]</li>
					</ul>
					<p>[Your&nbsp;Previous&nbsp;Job Title]</p>
					<p>[Previous&nbsp;Company Name]</p>
					<p>[2008&ndash;2012]</p>
					<ul>
						<li>List your jobs in reverse-chronological order. Start with your current or most recent position, then follow it with the one before it, and so on.</li>
						<li>As you go back in time, limit the number of bullet points under each entry. Employers are more interested in what you've been doing in the last few years, not in the dim and distant past.</li>
						<li>Don't list over 15 years of relevant work experience on your resume.</li>
					</ul>
					<div>
						<p><strong>Education</strong></p>
						<p>[BA/MA in Your Major]</p>
						<p>[University Name]</p>
						<p>[Graduation Year]</p>
						<ul>
							<li>Not much experience? Leverage your academic achievements. Include your GPA if it's higher than 3.5, mention extracurricular activities.</li>
							<li>If you have a lot of professional experience, limit your education section to your highest degree.</li>
						</ul>
					</div>
					<p><strong>Skills</strong></p>
					<p>[Skill&nbsp;#1: Advanced]</p>
					<p>[Skill&nbsp;#2: Advanced]</p>
					<p>[Skill&nbsp;#3: Basic]</p>
					<p><strong>Additional CV Sections</strong></p>
					<p>[Certifications]</p>
					<p>[Volunteer Experience]</p>
					<p>[Conference Participation]</p>
					<p>[Hobbies and Interests]</p>
				</div><a class="button button--big button--red mt-20--phone mt-40--tablet" href="#" title="Create a CV Now">Create a CV Now</a>
			</div>
		</section>


		<div class="two-column">
			<div class="two-column__wrapper grid">
				<section class="two-column__section two-column__section--best section section--medium">
					<div class="grid__row two-column__row">
						<div class="grid__column--phone--4 grid__column--tablet--5 two-column__text">
							<h2 class="two-column__title mb-15--phone">Best resume templates<br>
							for a job or academia</h2>
							<p class="two-column__description">A resume template is a blank form you fill in with contact information, work experience, skills, and education. Easier said than done. Most free MS Word resume template formats fall apart as soon as you start typing. But not Cv. Pick Cv and stop struggling. Focus on what matters: telling your story in a way that gets you the job.</p>
              <a class="hidden-button button button--red two-column__button" href="###" title="Create a CV Now">
                <span class="hidden-button__icon">
                  <svg height="28" viewbox="0 0 28 28" width="28" xmlns="http://www.w3.org/2000/svg">
							<path clip-rule="evenodd" d="M9.90732 13.1957l-1.23667 4.9502 4.94905-1.2378 7.4246-7.42471-3.7123-3.71233-7.42468 7.42464v0z" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path>
							<path d="M25.3762 5.15277l1.2366 1.23667c.3285.32822.513.77351.513 1.23783 0 .46432-.1845.90961-.513 1.23783L22.75 12.7268M21.0444 9.48342l4.3318-4.33067c.6832-.68338.6832-1.79113 0-2.4745l-1.2378-1.23783c-.6834-.683173-1.7912-.683173-2.4745 0l-4.3319 4.3295M21.875 16.6782v8.75c0 .9665-.7835 1.75-1.75 1.75h-17.5c-.9665 0-1.75-.7835-1.75-1.75V7.92822c0-.9665.7835-1.75 1.75-1.75h8.75" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path>
            </svg>
          </span>

          <span class="hidden-button__text">Create a CV Now</span></a>
						</div>
						<div class="grid__column--phone--4 grid__column--tablet--7 grid__column--desktop--6 grid__column--offset--desktop--1 two-column__image-wrapper">
							<img alt="Build a resume that gets you hired" class="two-column__image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-1@1x.png')}}" >
						</div>
					</div>
				</section>


				<section class="two-column__section two-column__section--design section section--medium">
					<div class="grid__row two-column__row">
						<div class="grid__column--phone--4 grid__column--tablet--5 two-column__text">
							<h2 class="two-column__title mb-15--phone">Attractive design</h2>
							<p class="two-column__description">Each resume format template is designed to draw attention to your assets. Highlight your career summary to hook the employer. Use icons to save valuable space. Pick a single or two column design to balance whitespace and wordcount. Show your mastery of skills by using graphs. Fit two pages on one.</p>
						</div>

						<div class="grid__column--phone--4 grid__column--tablet--7 grid__column--desktop--6 grid__column--offset--desktop--1 two-column__image-wrapper">
							<img alt="Build a resume that gets" class="two-column__image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-2@1x.png')}}">
					</div>
				</section>

				<section class="two-column__section two-column__section--ease section section--medium">
					<div class="grid__row two-column__row">
						<div class="grid__column--phone--4 grid__column--tablet--8 grid__column--desktop--6 grid__column--offset--desktop--1 two-column__text">
							<h2 class="two-column__title mb-30--phone">Ease of use</h2>
							<p class="two-column__description mb-40--phone mb-15--tablet mb-60--desktop">Go beyond downloadable resume templates that never seem to work. Pick a design you like, focus on the content, and let Cv take care of the rest. Our resume wizard will guide you through the process with helpful tips and examples. Building a resume with Cv is as simple as online shopping—just click an item you like and put in on your resume.</p>
							<div class="two-column__bottom-wrapper">
								<img alt="Build a resume that gets you hired" class="two-column__bottom-image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-3-1@1x.png')}}">
							</div>
						</div>

						<div class="grid__column--phone--4 grid__column--tablet--4 grid__column--desktop--5 two-column__image-wrapper">
							<img alt="Build a resume that gets you hired" class="two-column__image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-3-2@1x.png')}}">
						</div>
					</div>
				</section>


				<section class="two-column__section two-column__section--speed section section--medium">
					<div class="grid__row two-column__row">
						<div class="grid__column--phone--4 mb-40--phone">
							<h2 class="two-column__title mb-30--phone">Speed</h2>
							<p class="two-column__description">Our users have saved hours of valuable time by picking a resume format template on Cv. You should be out there building a career, not a resume. Save time by choosing a professional layout, follow our resume wizard app, and download an attractive resume in minutes. And create a cover letter while you’re at it to boost your chances of having your resume read.</p>
						</div>

						<div class="grid__column--phone--4 grid__column--offset--tablet--2 grid__column--tablet--8 two-column__image-wrapper">
							<img alt="Build a resume that gets you hired" class="two-column__image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-4@1x.png')}}">
						</div>
					</div>
				</section>


				<section class="two-column__section two-column__section--boost section section--medium">
					<div class="grid__row two-column__row">
						<div class="grid__column--phone--4 grid__column--tablet--5 grid__column--desktop--4 two-column__text">
							<h2 class="two-column__title mb-30--phone">Confidence boost</h2>
							<p class="two-column__description">It’s not just about looks. Filling out a resume with the Cv resume builder is fast, easy, and effective. Add optimized content with a single click to build your resume. Start from scratch or upload your old resume sample and give it a makeover in 5 minutes. Update the template, tweak the content with ready-made bullet points, get hired faster than anyone else.</p>
						</div>

						<div class="grid__column--phone--4 grid__column--offset--desktop--1 grid__column--tablet--7 two-column__image-wrapper">
							<img alt="Build a resume that gets you hired" class="two-column__image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-5@1x.png')}}">
						</div>
					</div>
				</section>

				<section class="two-column__section two-column__section--effectiveness section section--large">
					<div class="grid__row two-column__row">
						<div class="grid__column--phone--4 grid__column--tablet--5 grid__column--offset--tablet--1 grid__column--desktop--4 two-column__text">
							<h2 class="two-column__title mb-30--phone">Proven effectiveness</h2>
							<p class="two-column__description">Our customers haven’t simply updated their resumes. They’ve updated their lives. With the right balance between creativity and tradition, Cv’s templates for resumes have been optimized for readability and scanability. Hiring managers and recruitment software alike will have no trouble recognizing you for who you are—the perfect candidate.</p>
						</div>

						<div class="grid__column--phone--4 grid__column--tablet--5 grid__column--desktop--6 two-column__image-wrapper">
							<img alt="Build a resume that gets you hired" class="two-column__image" src="{{asset('resources/views/website/assets/templates/resume_templates/resume-templates-two-column-6@1x.png')}}">
						</div>
					</div>
				</section>

			</div>
		</div>
	</main>

	<section class="section--triangles section--large text--center lazyload">
		<div class="grid">
			<h3 class="text--white">Try Cv's professional resume builder now</h3>
      <a class="button button--big button--white mt-40--tablet mt-35--phone" href="##" title="Land Your Dream Job Now">Land Your Dream Job Now</a>
		</div>
	</section>

@endsection