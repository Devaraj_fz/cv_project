<style type="text/css">
	
@media only screen and (max-width: 1139px) {
header{

	display: none !important;
}
}

@media only screen and (min-width: 1140px) {
.navbar{

	display: none !important;
}
}



</style>





	<header class="header">
		<div class="header__grid grid">
			<div class="header__logo">
				<a class="logo" href="" title="Cv - Home page">
          <svg class="logo" height="34" viewbox="0 0 461 227" width="70">
				<defs>
					<lineargradient id="b" x1="45.207%" x2="45.207%" y1="0%" y2="44.814%">
						<stop offset="0%" stop-color="#FFF" stop-opacity=".2"></stop>
						<stop offset="100%" stop-color="#FFF" stop-opacity="0"></stop>
					</lineargradient>
					<path d="M75.514 86.473L0 43.493 75.514.513l75.514 42.98v85.96l-75.514-42.98z" id="a"></path>
					<lineargradient id="d" x1="50%" x2="25.242%" y1="67.92%" y2="62.042%">
						<stop offset="0%" stop-opacity="0"></stop>
						<stop offset="100%"></stop>
					</lineargradient>
					<path d="M113.271 91.696l42.98 75.514h-85.96z" id="c"></path>
				</defs>
				<g fill="none" fill-rule="evenodd">
					<path d="M-19-13h500v250H-19z" fill="none"></path>
					<g transform="translate(309.536 .357)">
						<use fill="#3983FA" xlink:href="#a"></use>
						<use fill="url(#b)" xlink:href="#a"></use>
					</g>
					<g transform="rotate(90 267.86 284.4)">
						<use fill="#0667D0" xlink:href="#"></use>
						<use fill="url(#d)" fill-opacity=".21" xlink:href="#c"></use>
					</g>
                            <!-- LOGONAME    -->
                            
				</g></svg></a>
			</div>
			<div class="header__menu">
				<nav class="menu">
					<ul class="menu__list">
						<li class="menu__item submenu__wrapper">
							<span class="menu__link">Resume</span>
							<div class="submenu">
								<div class="submenu__title submenu__title--resumebuilder">
									<div class="submenu__text">
										<a class="submenu__title__link" href="resume-builder">
											<span class="text--blue">Resume</span> Builder</a> 
                    <span class="submenu__title__desc">Create a resume in 5 minutes. Get the job you want.</span>
									</div>
								</div>

								<ul class="submenu__list">
									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M9 12.75H2.25a1.5 1.5 0 0 1-1.5-1.5v-9a1.5 1.5 0 0 1 1.5-1.5h18a1.5 1.5 0 0 1 1.5 1.5v6M23.25 17.25a6 6 0 1 1-6-6"></path>
										<path class="a" d="M23.25 12.749l-5.47 5.47a.749.749 0 0 1-1.06 0L15 16.5M5.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375M11.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375M17.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375"></path>
									</svg>
								</span>
                    <a class="submenu__link" href="resume-templates">Resume Templates</a> 
                    <span class="submenu__desc">Find the perfect resume template.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M5.25 2.249h-3a1.5 1.5 0 0 0-1.5 1.5v18a1.5 1.5 0 0 0 1.5 1.5h19.5a1.5 1.5 0 0 0 1.5-1.5v-18a1.5 1.5 0 0 0-1.5-1.5h-10.5"></path>
										<path class="a" d="M11.25 11.249L8.25 9l-3 2.25V1.5A.75.75 0 0 1 6 .749h4.5a.75.75 0 0 1 .75.75zM5.25 18.749h10.5M5.25 14.249h13.5M18.75 9.749h-4.5"></path>
									</svg>
								</span>
                    <a class="submenu__link" href="resume-example">Resume Examples</a> 
                    <span class="submenu__desc">See perfect resume samples that get jobs.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M13.045 18.636l-3.712.53.53-3.712 9.546-9.546a2.25 2.25 0 0 1 3.182 3.182z"></path>
										<rect class="a" height="3" rx=".75" ry=".75" width="7.5" x="5.25" y=".749"></rect>
										<path class="a" d="M12.75 2.249h3a1.5 1.5 0 0 1 1.5 1.5M17.25 18.749v3a1.5 1.5 0 0 1-1.5 1.5H2.25a1.5 1.5 0 0 1-1.5-1.5v-18a1.5 1.5 0 0 1 1.5-1.5h3M5.25 8.249h7.5M5.25 12.749h3"></path></svg></span>
                    <a class="submenu__link" href="#">Resume Format</a> 
                    <span class="submenu__desc">Pick the right resume format for your situation.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<circle class="a" cx="5.249" cy="15.75" r="3"></circle>
										<path class="a" d="M9.473 23.25a4.474 4.474 0 0 0-8.449 0M21.516 14.589a3 3 0 0 1-5.192 2.927M22.973 23.25a4.474 4.474 0 0 0-8.449 0M21 .75h.75a1.5 1.5 0 0 1 1.5 1.5v8.25a1.5 1.5 0 0 1-1.5 1.5h-4.5l-4.5 4.5V12h-3a1.5 1.5 0 0 1-1.5-1.5V2.25a1.5 1.5 0 0 1 1.5-1.5h1.5M13.874 3.375a1.875 1.875 0 1 1 1.875 1.875"></path>
										<path class="a" d="M15.749 8.25a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375"></path></svg></span>
                    <a class="submenu__link" href="#">How to Write a Resume</a> 
                    <span class="submenu__desc">Learn how to make a resume that gets interviews.</span>
									</li>
                  
									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<circle class="a" cx="17.25" cy="17.261" r="6"></circle>
										<path class="a" d="M15.375 16.136a1.875 1.875 0 1 1 1.875 1.875M17.25 20.261a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375M5.25 10.511h5.25M5.25 14.261h3M5.25 18.011h3M9.75 23.261h-7.5a1.5 1.5 0 0 1-1.5-1.5V6.011a1.5 1.5 0 0 1 1.5-1.5H6a3.75 3.75 0 0 1 7.5 0h3.75a1.5 1.5 0 0 1 1.5 1.5v2.25"></path>
										<path class="a" d="M9.75 3.761a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375"></path></svg></span>
                    <a class="submenu__link" href="#">Resume Help</a> 
                    <span class="submenu__desc">Improve your resume with help from expert guides.</span>
									</li>
								</ul>
                <a class="button button--red submenu__button" href="#">Create a resume now</a>
							</div>
						</li>

                                                <!-- CV TAB -->

						<li class="menu__item submenu__wrapper">
							<span class="menu__link">CV</span>
							<div class="submenu">
								<div class="submenu__title submenu__title--resumebuilder">
									<div class="submenu__text">
										<a class="submenu__title__link" href="cv-builder"><span class="text--blue">CV</span> Builder</a> 
                    <span class="submenu__title__desc">Create a CV in 5 minutes. Get the job you want.</span>
									</div>
								</div>
								<ul class="submenu__list">
									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M9 12.75H2.25a1.5 1.5 0 0 1-1.5-1.5v-9a1.5 1.5 0 0 1 1.5-1.5h18a1.5 1.5 0 0 1 1.5 1.5v6M23.25 17.25a6 6 0 1 1-6-6"></path>
										<path class="a" d="M23.25 12.749l-5.47 5.47a.749.749 0 0 1-1.06 0L15 16.5M5.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375M11.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375M17.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375"></path>
									</svg>
								</span>
                    						<a class="submenu__link" href="cv-templates">CV Templates</a> 
                    							<span class="submenu__desc">Find the perfect CV template.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M5.25 2.249h-3a1.5 1.5 0 0 0-1.5 1.5v18a1.5 1.5 0 0 0 1.5 1.5h19.5a1.5 1.5 0 0 0 1.5-1.5v-18a1.5 1.5 0 0 0-1.5-1.5h-10.5"></path>
										<path class="a" d="M11.25 11.249L8.25 9l-3 2.25V1.5A.75.75 0 0 1 6 .749h4.5a.75.75 0 0 1 .75.75zM5.25 18.749h10.5M5.25 14.249h13.5M18.75 9.749h-4.5"></path>
									</svg>
								</span>
                    						<a class="submenu__link" href="cv-example">CV Examples</a> 
                    							<span class="submenu__desc">See perfect CV samples that get jobs.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M13.045 18.636l-3.712.53.53-3.712 9.546-9.546a2.25 2.25 0 0 1 3.182 3.182z"></path>
										<rect class="a" height="3" rx=".75" ry=".75" width="7.5" x="5.25" y=".749"></rect>
										<path class="a" d="M12.75 2.249h3a1.5 1.5 0 0 1 1.5 1.5M17.25 18.749v3a1.5 1.5 0 0 1-1.5 1.5H2.25a1.5 1.5 0 0 1-1.5-1.5v-18a1.5 1.5 0 0 1 1.5-1.5h3M5.25 8.249h7.5M5.25 12.749h3"></path></svg></span>
                    						<a class="submenu__link" href="#">CV Format</a> 
												<span class="submenu__desc">Pick the right format for your situation.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<circle class="a" cx="5.249" cy="15.75" r="3"></circle>
										<path class="a" d="M9.473 23.25a4.474 4.474 0 0 0-8.449 0M21.516 14.589a3 3 0 0 1-5.192 2.927M22.973 23.25a4.474 4.474 0 0 0-8.449 0M21 .75h.75a1.5 1.5 0 0 1 1.5 1.5v8.25a1.5 1.5 0 0 1-1.5 1.5h-4.5l-4.5 4.5V12h-3a1.5 1.5 0 0 1-1.5-1.5V2.25a1.5 1.5 0 0 1 1.5-1.5h1.5M13.874 3.375a1.875 1.875 0 1 1 1.875 1.875"></path>
										<path class="a" d="M15.749 8.25a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375"></path></svg></span>
											<a class="submenu__link" href="#">How to Write a CV</a> 
											<span class="submenu__desc">Learn how to make a CV that gets interviews.</span>
									</li>
									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<circle class="a" cx="17.25" cy="17.261" r="6"></circle>
										<path class="a" d="M15.375 16.136a1.875 1.875 0 1 1 1.875 1.875M17.25 20.261a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375M5.25 10.511h5.25M5.25 14.261h3M5.25 18.011h3M9.75 23.261h-7.5a1.5 1.5 0 0 1-1.5-1.5V6.011a1.5 1.5 0 0 1 1.5-1.5H6a3.75 3.75 0 0 1 7.5 0h3.75a1.5 1.5 0 0 1 1.5 1.5v2.25"></path>
										<path class="a" d="M9.75 3.761a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375"></path></svg></span>
                   							 <a class="submenu__link" href="#">CV Help</a> 
                    						 <span class="submenu__desc">Improve your CV with help from expert guides.</span>
									</li>
								</ul><a class="button button--red submenu__button" href="#">Create a CV</a>
							</div>
						</li>



						<li class="menu__item submenu__wrapper">
							<span class="menu__link">Resources</span>
							<div class="submenu">
								<div class="submenu__title submenu__title--coverletter">
									<div class="submenu__text">
										<a class="submenu__title__link" href="#">
					                      <span class="text--blue">Cover Letter</span> Builder</a> 
									<span class="submenu__title__desc">Create a cover letter in 5 minutes. Get the job you want.</span>
									</div>
								</div>

								<ul class="submenu__list">
									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M9 12.75H2.25a1.5 1.5 0 0 1-1.5-1.5v-9a1.5 1.5 0 0 1 1.5-1.5h18a1.5 1.5 0 0 1 1.5 1.5v6M23.25 17.25a6 6 0 1 1-6-6"></path>
										<path class="a" d="M23.25 12.749l-5.47 5.47a.749.749 0 0 1-1.06 0L15 16.5M5.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375M11.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375M17.25 6.375a.375.375 0 1 1-.375.375.374.374 0 0 1 .375-.375"></path>
									</svg>
								</span>
				                    <a class="submenu__link" href="#">Cover Letter Templates</a> 
				                    <span class="submenu__desc">Find the perfect cover letter template.</span>
								</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M5.25 2.249h-3a1.5 1.5 0 0 0-1.5 1.5v18a1.5 1.5 0 0 0 1.5 1.5h19.5a1.5 1.5 0 0 0 1.5-1.5v-18a1.5 1.5 0 0 0-1.5-1.5h-10.5"></path>
										<path class="a" d="M11.25 11.249L8.25 9l-3 2.25V1.5A.75.75 0 0 1 6 .749h4.5a.75.75 0 0 1 .75.75zM5.25 18.749h10.5M5.25 14.249h13.5M18.75 9.749h-4.5"></path></svg></span>
                    <a class="submenu__link" href="#">Cover Letter Examples</a> 
                    <span class="submenu__desc">See perfect cover letter samples that get jobs.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<path class="a" d="M13.045 18.636l-3.712.53.53-3.712 9.546-9.546a2.25 2.25 0 0 1 3.182 3.182z"></path>
										<rect class="a" height="3" rx=".75" ry=".75" width="7.5" x="5.25" y=".749"></rect>
										<path class="a" d="M12.75 2.249h3a1.5 1.5 0 0 1 1.5 1.5M17.25 18.749v3a1.5 1.5 0 0 1-1.5 1.5H2.25a1.5 1.5 0 0 1-1.5-1.5v-18a1.5 1.5 0 0 1 1.5-1.5h3M5.25 8.249h7.5M5.25 12.749h3"></path></svg></span>
                    <a class="submenu__link" href="#">Cover Letter Format</a> 
                    <span class="submenu__desc">Pick the right format for your situation.</span>
									</li>

									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<circle class="a" cx="5.249" cy="15.75" r="3"></circle>
										<path class="a" d="M9.473 23.25a4.474 4.474 0 0 0-8.449 0M21.516 14.589a3 3 0 0 1-5.192 2.927M22.973 23.25a4.474 4.474 0 0 0-8.449 0M21 .75h.75a1.5 1.5 0 0 1 1.5 1.5v8.25a1.5 1.5 0 0 1-1.5 1.5h-4.5l-4.5 4.5V12h-3a1.5 1.5 0 0 1-1.5-1.5V2.25a1.5 1.5 0 0 1 1.5-1.5h1.5M13.874 3.375a1.875 1.875 0 1 1 1.875 1.875"></path>
										<path class="a" d="M15.749 8.25a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375"></path></svg></span>
                    <a class="submenu__link" href="#">How to Write a Cover Letter</a> 
                    <span class="submenu__desc">Learn how to make a cover letter that gets interviews.</span>
									</li>
                  
									<li class="submenu__item">
										<span class="submenu__icon"><svg viewbox="0 0 24 24">
										 
										<circle class="a" cx="17.25" cy="17.261" r="6"></circle>
										<path class="a" d="M15.375 16.136a1.875 1.875 0 1 1 1.875 1.875M17.25 20.261a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375M5.25 10.511h5.25M5.25 14.261h3M5.25 18.011h3M9.75 23.261h-7.5a1.5 1.5 0 0 1-1.5-1.5V6.011a1.5 1.5 0 0 1 1.5-1.5H6a3.75 3.75 0 0 1 7.5 0h3.75a1.5 1.5 0 0 1 1.5 1.5v2.25"></path>
										<path class="a" d="M9.75 3.761a.375.375 0 1 1-.375.375.375.375 0 0 1 .375-.375"></path></svg></span>
                    <a class="submenu__link" href="#">Cover Letter Help</a> 
                    <span class="submenu__desc">Boost your chances of having your resume read with our help.</span>
									</li>
								</ul>
                <a class="button button--red submenu__button" href="#">Create a Cover Letter Now</a>
							</div>
						</li>
					</ul>
				</nav><a class="button button--medium button--light-blue header__button" href="#" rel="noopener noreferrer" target="_blank" title="My Account">My Account</a>
			</div>




			
			<div class="header__hamburger" >
				<div class="hamburger" >
					<div class="hamburger__box">
						<div class="hamburger__inner"></div>
					</div>
				</div>
			</div>
		</div>




	</header>




<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


	


<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
  <a class="navbar-brand" href="#">
  	<img src="https://www.friendzion.com/assets/v3/FZ.png" width="135">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse text-center" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Resume</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Cv</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Resources
        </a>
        <div class="dropdown-menu text-center" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
  </div>
</nav>

