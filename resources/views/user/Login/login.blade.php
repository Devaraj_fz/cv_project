
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login </title>
    <link rel="stylesheet" type="text/css" href="{{ asset('login_asset/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('login_asset/css/fontawesome-all.min.css') }} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('login_asset/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('login_asset/css/iofrm-theme4.css') }} ">

    <link href="{{ asset('assets/fontawesome/css/all.css')}}" rel="stylesheet">
</head>
<body>
    <div class="form-body">
       <!--  <div class="website-logo">
            <a href="{{ url('') }}">
                <div class="logo">
                    <img class="logo-size" src="{{ asset('login_asset/images/logo-light.svg') }}" alt="">
                </div>
            </a>
        </div> -->
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="{{ asset('assets/resume-png.png') }}" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>User Portal</h3>
                        <p>Start Build Your own resume</p>
                        <div class="page-links">
                            <a href="{{ url('user/login') }}" class="active">Login</a><a href="{{ url('user/register') }}">Register</a>
                        </div>
                        <form>
                            <input class="form-control" type="text" name="username" placeholder="E-mail Address" required>
                            <input class="form-control" type="password" name="password" placeholder="Password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Login</button> <a href="{{ url('contractor/login') }}">Forget password?</a>
                            </div>
                        </form>
                        <div class="other-links">
                        <span>Or login with</span><a href="{{ url('#') }}"><i class="fab fa-linkedin"></i> Facebook</a><a href="{{ url('#') }}"><i class="fab fa-google-plus-g"></i> Google</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('login_asset/js/jquery.min.js') }}"></script>
<script src="{{ asset('login_asset/js/popper.min.js') }}"></script>
<script src="{{ asset('login_asset/js/bootstrap.min.js') }} "></script>
<script src="{{ asset('login_asset/js/main.js') }} "></script>
</body>
</html>