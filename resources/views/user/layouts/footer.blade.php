 <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright © 2021 Friendzion Technologies. All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Cv<i class="fas fa-heart"></i></p>
              </div>
            </div>
          </div>
        </footer>
