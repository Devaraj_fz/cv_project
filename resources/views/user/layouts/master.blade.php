<!DOCTYPE html>
<html lang="en">



<head>
    <title> Cv </title>
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="" type="image/x-icon">
    <!-- Google font-->
 
 <style id="" media="all">@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: url(https://colorlib.com/fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFVZ0e.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: url(https://colorlib.com/fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOUuhs.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: url(https://colorlib.com/fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOUuhs.ttf) format('truetype');
}
</style>

  <link href="{{asset('assets/adminassets/files/fontawesome/css/all.css')}}" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/bower_components/bootstrap/css/bootstrap.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/assets/icon/themify-icons/themify-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/assets/icon/icofont/css/icofont.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/assets/icon/feather/css/feather.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/assets/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/adminassets/files/assets/css/jquery.mCustomScrollbar.css')}}">


</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>




    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">



<!-- header -->

@include('user.layouts.header')


<!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">


                    <!-- sidebar -->


@include('user.layouts.sidebar')
                    <!-- content -->

@yield('content')





                      </div>
            </div>
        </div>
    </div>

  


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    $(function() {
     $(".alert").fadeOut(5000);
});
</script>



<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/modernizr/js/modernizr.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

<script src="{{asset('assets/adminassets/files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/assets/pages/data-table/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/adminassets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/i18next/js/i18next.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>

<script src="{{asset('assets/adminassets/files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
<script src="{{asset('assets/adminassets/files/assets/js/pcoded.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/assets/js/vartical-layout.min.js')}}"></script>
<script src="{{asset('assets/adminassets/files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/adminassets/files/assets/js/script.js')}}"></script>




  

</body>



</html>
