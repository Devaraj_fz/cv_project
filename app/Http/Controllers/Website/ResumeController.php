<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResumeController extends Controller
{


   public function index(){
       return view('index');
   }
   public function resumeBuilder(){
       return view('website.pages.resume-builder');
   }

   public function resumeExamples(){
    return view('website.pages.resume-examples');
}


   public function resumeTemplates(){
    return view('website.pages.resume-templates');
}

   public function cvBuilder(){
    return view('website.pages.CV.CV-builder');
}

   public function cvExamples(){
    return view('website.pages.CV.CV-examples');
}

   public function cvTemplates(){
    return view('website.pages.CV.cv-templates');
}
}  
